#!/usr/bin/env sh

set -euf

error() {
    >&2 echo "$1"
    exit 1
}

help() {
    cat <<EOF
$0: - update the production.

This program updates Trivabble to the latest version tagged for production, and copies and prepares Trivabble for use in production.
Trivabble is composed of two parts: the server and the client.
The client is to be served by a regular HTTP server and lives in the public folder.
The server is to be stored somewhere on your system that is not served by your HTTP server.

Parameters:
    --prod-public-dir DIR: the path to the public directory, from which your HTTP server will serve the client
    --prod-server-dir DIR: the path to the server directory, not served by your HTTP server.
EOF
    exit 0
}

if [ "$(id -u)" = "0" ]; then
   error "Not running as root. Please run me as your webserver's user for instance, using something like sudo -H -u www-data $0 $1"
fi

while ! [ -z "${1+x}" ]; do
    if [ "$1" = "--prod-public-dir" ]; then
        PROD_PUBLIC_DIR=$2
        shift
        shift
    elif [ "$1" = "--prod-server-dir" ]; then
        PROD_SERVER_DIR=$2
        shift
        shift
    elif [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
        help
    else
        error "Unrecognized argument $1. Type --help for more information."
    fi
done

if [ -z "${PROD_SERVER_DIR+x}" ]; then
   error "The server production directory is not set. Type --help for more information."
fi

if [ -z "${PROD_PUBLIC_DIR+x}" ]; then
   error "The public production directory is not set. Type --help for more information."
fi

if ! [ -d "$PROD_SERVER_DIR" ]; then
   error "The server production directory $PROD_SERVER_DIR does not exist. I give up."
fi

if ! [ -d "$PROD_PUBLIC_DIR" ]; then
   error "The public production directory $PROD_PUBLIC_DIR does not exist. I give up."
fi

WORKING_DIRECTORY="$(dirname "$(dirname "$0")")"

if ! [ -d "$WORKING_DIRECTORY/.git" ]; then
    error "Not a git repository. Cannot update."
fi

if [ -f "$WORKING_DIRECTORY/server/games.backup.json" ] || [ -f "$WORKING_DIRECTORY/games.backup.json" ]; then
    error "Your working copy has a 'games.backup.json' file. I give up."
fi

if ! [ -z "$(git status --porcelain)" ]; then
    error "Your git repository is not clean. I am not doing anything."
fi

# Thx https://stackoverflow.com/questions/17414104/git-checkout-latest-tag

echo "Fetching updates from git"
git fetch
new_version="$(git tag --list '[prod]*' --sort=v:refname | tail -1)"

echo "Checking out ${new_version}..."
git checkout "$new_version"

echo "Setting up production mode..."
"$(dirname $0)"/setup_prod.sh

echo "Sending to production..."
rsync -avp --delete --exclude 'config.js' "$WORKING_DIRECTORY/public/" "$PROD_PUBLIC_DIR/"
rsync -avp --delete --exclude 'config.js' "$WORKING_DIRECTORY/server/" "$PROD_SERVER_DIR/"

echo echo "Resetting the directory..."
git reset --hard "$new_version"
